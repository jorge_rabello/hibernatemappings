package com.concrete.chalenge.service;

import com.concrete.chalenge.error.exception.UserNotFoundException;
import com.concrete.chalenge.model.dto.UserRequestDTO;
import com.concrete.chalenge.model.dto.UserResponseDTO;
import com.concrete.chalenge.model.entity.User;
import com.concrete.chalenge.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository repository;
    private final ModelMapper modelMapper;

    @Autowired
    public UserService(UserRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    public UserResponseDTO create(UserRequestDTO userRequestDTO) {
        User user = modelMapper.map(userRequestDTO, User.class);
        User savedUser = repository.save(user);
        return modelMapper.map(savedUser, UserResponseDTO.class);
    }

    public UserResponseDTO getById(Long id) {
        Optional<User> optionalUser = repository.findById(id);
        if (optionalUser.isPresent()) {
            return modelMapper.map(optionalUser.get(), UserResponseDTO.class);
        }
        throw new UserNotFoundException("Usuário não encontrado");
    }
}
