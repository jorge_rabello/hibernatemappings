package com.concrete.chalenge.model.dto;

import java.util.Objects;

public class PhonesRequestDTO {

    private String ddd;
    private String number;

    public PhonesRequestDTO() {
    }

    public PhonesRequestDTO(String ddd, String number) {
        this.ddd = ddd;
        this.number = number;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhonesRequestDTO that = (PhonesRequestDTO) o;
        return Objects.equals(ddd, that.ddd) && Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ddd, number);
    }
}
