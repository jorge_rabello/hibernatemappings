package com.concrete.chalenge.model.dto;

import java.util.Objects;

public class PhonesResponseDTO {

    private Long id;
    private String ddd;
    private String number;

    public PhonesResponseDTO() {
    }

    public PhonesResponseDTO(Long id, String ddd, String number) {
        this.id = id;
        this.ddd = ddd;
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhonesResponseDTO phonesResponseDTO = (PhonesResponseDTO) o;
        return Objects.equals(id, phonesResponseDTO.id) && Objects.equals(ddd, phonesResponseDTO.ddd) && Objects.equals(number, phonesResponseDTO.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ddd, number);
    }
}
