package com.concrete.chalenge.model.dto;


import java.util.List;
import java.util.Objects;

public class UserRequestDTO {

    private String name;
    private String email;
    private List<PhonesRequestDTO> phones;

    public UserRequestDTO() {
    }

    public UserRequestDTO(String name, String email, List<PhonesRequestDTO> phones) {
        this.name = name;
        this.email = email;
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public List<PhonesRequestDTO> getPhones() {
        return phones;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhones(List<PhonesRequestDTO> phones) {
        this.phones = phones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRequestDTO that = (UserRequestDTO) o;
        return Objects.equals(name, that.name) && Objects.equals(email, that.email) && Objects.equals(phones, that.phones);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, phones);
    }

    @Override
    public String toString() {
        return "UserRequestDTO{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phones=" + phones +
                '}';
    }
}
