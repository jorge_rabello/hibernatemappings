package com.concrete.chalenge.model.dto;

import java.util.List;
import java.util.Objects;

public class UserResponseDTO {

    private Long id;
    private String name;
    private String email;
    private List<PhonesResponseDTO> phones;

    public UserResponseDTO() {
    }

    public UserResponseDTO(Long id, String name, String email, List<PhonesResponseDTO> phones) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phones = phones;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<PhonesResponseDTO> getPhones() {
        return phones;
    }

    public void setPhones(List<PhonesResponseDTO> phones) {
        this.phones = phones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponseDTO userResponseDTO = (UserResponseDTO) o;
        return Objects.equals(id, userResponseDTO.id) && Objects.equals(name, userResponseDTO.name) && Objects.equals(email, userResponseDTO.email) && Objects.equals(phones, userResponseDTO.phones);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, phones);
    }
}
